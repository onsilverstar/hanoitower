/*1. display tower in html and css 
        *Rod 
        *bottom 
        *disks
        *heading 
        *new game button
        *first rod should have all disks sorted order

  2. Move disks 
        * click handler 
        * click handler on tower not disks
                    => we will check the move is valid or not   
        * current move tracker 

3. we will move the disks unless win conditions
        *after every move we will check wethere user win or not 
        *two towers are empty and only  3rd tower has children 

*/

//first of all 
let towerStart  = document.getElementById("tower1");
let towerOffSet = document.getElementById("tower2");
let towerEnd    = document.getElementById("tower3");
let diskReadyToMove=false;
let towerOffSetClickedCounter = 0;
let towerEndClickedCounter = 0;
let main=document.getElementById("towerFlexContainer");
diskToMove = null;
let childCount=0;
let currTowerDiskSize=0;
let toMoveDiskSize=0;
let towerStartClickedCounter = 0;
let winDisplay=document.getElementById("win")
        towerStart.onclick = function(evt)
        {
                towerStartClickedCounter++; 
                childCount = evt.currentTarget.childElementCount;      
                if(diskReadyToMove===true&&evt.currentTarget.childElementCount===0)
                {
                        diskToMove.classList.remove("goUp");
                        evt.currentTarget.appendChild(diskToMove);
                        diskReadyToMove=false; 
                        return
                }
                else if(diskReadyToMove===true&&evt.currentTarget.childElementCount>0)
                {
                        currTowerDiskSize=parseInt(towerStart.children[childCount-1].dataset.disk);
                        toMoveDiskSize=parseInt(diskToMove.dataset.disk);
                        if(currTowerDiskSize<=toMoveDiskSize)
                        {
                                diskToMove.classList.remove("goUp");
                                evt.currentTarget.appendChild(diskToMove);
                                diskReadyToMove=false;
                                return
                        }
                        else
                        {
                                diskToMove.classList.remove("goUp");
                                diskReadyToMove=false;
                                return;
                        }
                }
                else
                {
                        if(evt.currentTarget.childElementCount>0&&diskReadyToMove===false)
                        {       diskReadyToMove=true;     
                                diskToMove = evt.currentTarget.children[childCount-1];
                                evt.currentTarget.children[childCount-1].classList.add("goUp");             
                        }
                        else if (evt.currentTarget.childElementCount>0&&diskReadyToMove===true)
                        { 
                                evt.currentTarget.children[childCount-1].classList.remove("goUp");
                                evt.currentTarget.appendChild(diskToMove);
                                diskReadToMove=false;      
                        }
                }
                return;
        }
        towerOffSet.onclick = function(evtOffset)
        {
                towerOffSetClickedCounter++;
                childCount = evtOffset.currentTarget.childElementCount;     

               
                if(diskReadyToMove===true&&evtOffset.currentTarget.childElementCount===0)
                {
                        diskToMove.classList.remove("goUp");
                        evtOffset.currentTarget.appendChild(diskToMove);
                        diskReadyToMove=false; 
                        return
                }
                else if(diskReadyToMove===true&&evtOffset.currentTarget.childElementCount>0)
                {
                        currTowerDiskSize=parseInt(towerOffSet.children[childCount-1].dataset.disk);
                        toMoveDiskSize=parseInt(diskToMove.dataset.disk);
                        if(currTowerDiskSize<=toMoveDiskSize)
                        {
                                diskToMove.classList.remove("goUp");
                                evtOffset.currentTarget.appendChild(diskToMove);
                                diskReadyToMove=false;
                        }
                }
                
                else
                {
                        if(evtOffset.currentTarget.childElementCount>0&&diskReadyToMove===false)
                        {       diskReadyToMove=true;     
                                diskToMove = evtOffset.currentTarget.children[childCount-1];
                                evtOffset.currentTarget.children[childCount-1].classList.add("goUp");             
                        }
                        else if (evtOffset.currentTarget.childElementCount>0&&diskReadyToMove===true)
                        { 
                                evt.currentTarget.children[childCount-1].classList.remove("goUp");
                                evt.currentTarget.appendChild(diskToMove);
                                diskReadToMove=false;      
                        }
                        
                }   
                return;
        }
        towerEnd.onclick = function(evt)
        {
                towerEndClickedCounter++;
                childCount = evt.currentTarget.childElementCount; 
                if(diskReadyToMove===true&&evt.currentTarget.childElementCount===0)
                {
                        diskToMove.classList.remove("goUp");
                        evt.currentTarget.appendChild(diskToMove);
                        diskReadyToMove=false; 
                        return
                }
                else if(diskReadyToMove===true&&evt.currentTarget.childElementCount>0)
                {
                        currTowerDiskSize=parseInt(towerEnd.children[childCount-1].dataset.disk);
                        toMoveDiskSize=parseInt(diskToMove.dataset.disk);
                        if(currTowerDiskSize<=toMoveDiskSize)
                        {
                                diskToMove.classList.remove("goUp");
                                evt.currentTarget.appendChild(diskToMove);
                                if(evt.currentTarget.childElementCount===4)
                                        {
                                                winDisplay.style.display="block"
                                        } 
                                diskReadyToMove=false;
                                return
                        }
                }
                
                else
                {
                        if(evt.currentTarget.childElementCount>0&&diskReadyToMove===false)
                        {       diskReadyToMove=true;     
                                diskToMove = evt.currentTarget.children[childCount-1];
                                evt.currentTarget.children[childCount-1].classList.add("goUp");             
                        }
                        else if (evt.currentTarget.childElementCount>0&&diskReadyToMove===true)
                        { 
                                evt.currentTarget.children[childCount-1].classList.remove("goUp");
                                evt.currentTarget.appendChild(diskToMove);
                                if(evt.currentTarget.childElementCount===4)
                                        {
                                                        winDisplay.style.display="block"
                                        } 
                                diskReadToMove=false; 
                                return     
                        }
                        
                } 
                
                return;
        
        }

